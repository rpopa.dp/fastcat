# FastCat

Inspired by [fcat](https://github.com/mre/fcat)

# Building

```sh
make
```

# Running

```sh
./fastcat <...files>
```

# Known issues

If `splice()` is used then it won't work if:
  * you redirect output from the program to a file in append mode (`./fastcat /dev/urandom >> /dev/null`)
  * output is done to stdout

Both issues are described in the man pages of `splice()` under `ERRORS`, specifically `EINVAL`.
> Target  filesystem doesn't support splicing; target file is opened in append mode; neither of the descriptors refers to a pipe; or offset given for nonseekable device.
